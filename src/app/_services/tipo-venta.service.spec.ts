import { TestBed } from '@angular/core/testing';

import { TipoVentaService } from './tipo-venta.service';

describe('TipoVentaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipoVentaService = TestBed.get(TipoVentaService);
    expect(service).toBeTruthy();
  });
});
