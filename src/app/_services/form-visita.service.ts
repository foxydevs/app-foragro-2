import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError} from 'rxjs/operators';
import { path } from '../config.module';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class FormVisitaService {
  private basePath:string = path.path;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  
  constructor(private http: HttpClient,
    private httpIonic: HTTP) { }

  //HANDLE ERROR
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('Un error ha ocurrido:', error.error.message);
    } else {
      console.error(
      `Backend returned code ${error.status}, ` +
      `body was: `, error.error);
    }
    return throwError(
      'Algo malo sucedio, por favor intentarlo más tarde D:');
  };

  //GET ALL
  getAll() : Observable<any> {
    let url = `${this.basePath}formvisita`;
    return this.http.get(url)
    .pipe(
      catchError(this.handleError)
    );
  }

  //GET SINGLE
  getSingle(id:number) : Observable<any> {
    let url = `${this.basePath}formvisita/${id}`;
    return this.http.get(url)
    .pipe(
      catchError(this.handleError)
    );
  }

  //CREATE
  public create(data:any) : Observable<any> {
    let url = `${this.basePath}formvisita`;
    return this.http.post(url, data, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

  //CREATE
  public update(data:any) : Observable<any> {
    let url = `${this.basePath}formvisita/${data.id}`;
    return this.http.put(url, data, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

  //DELETE
  delete(id:number) : Observable<any> {
    let url = `${this.basePath}formvisita/${id}`;
    return this.http.delete(url)
    .pipe(
      catchError(this.handleError)
    );
  }

  //GET SINGLE
  getAllClient(id:number) : Observable<any> {
    let url = `${this.basePath}clientes/${id}/formvisita`;
    return this.http.get(url)
    .pipe(
      catchError(this.handleError)
    );
  }

  //GET SINGLE
  getNear(id:number, state:number, fecha:any) : Observable<any> {
    let url = `${this.basePath}clientes/${id}/formvisita/${state}?fecha=${fecha}`;
    return this.http.get(url)
    .pipe(
      catchError(this.handleError)
    );
  }

  //SEND MESSAGE
  sendMail(idUsuario:number, fechaInicio:any, fechaFin:any, state:any) : Observable<any> {
    //http://backend.foxylabs.xyz/foragro/public/api/report/formdiario/2/2019-07-29/2019-07-31/0
    //http://backend.foxylabs.xyz/foragro/public/api/report/mailreportdiario/{{usuariosEmail}}/{{fechaIncialEmail}}/{{fechaFinalEmail}}/0
    let url = `${this.basePath}report/mailreportdiario/${idUsuario}/${fechaInicio}/${fechaFin}/${state}`;
    return this.http.get(url)
    .pipe(
      catchError(this.handleError)
    );
  }

  //CREATE
  initRoute(data:any) : Observable<any> {
    let url = `http://alquimistapp.com/backend/foragro_rutas/index.php/all/crear_ruta_enc`;
    return this.http.post(url, data, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )


  }

  //CREATE
  public createRowRoute(data:any) : Observable<any> {
    let url = `https://alquimistapp.com/backend/foragro_rutas/index.php/all/crear_ruta_det`;
    return this.http.post(url, data, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

  //CREATE
  public endRoute(data:any) : Observable<any> {
    let url = `https://alquimistapp.com/backend/foragro_rutas/index.php/all/terminar_ruta_historial`;
    return this.http.post(url, data, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

}
