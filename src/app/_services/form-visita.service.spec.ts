import { TestBed } from '@angular/core/testing';

import { FormVisitaService } from './form-visita.service';

describe('FormVisitaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FormVisitaService = TestBed.get(FormVisitaService);
    expect(service).toBeTruthy();
  });
});
