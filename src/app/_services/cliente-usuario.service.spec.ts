import { TestBed } from '@angular/core/testing';

import { ClienteUsuarioService } from './cliente-usuario.service';

describe('ClienteUsuarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClienteUsuarioService = TestBed.get(ClienteUsuarioService);
    expect(service).toBeTruthy();
  });
});
