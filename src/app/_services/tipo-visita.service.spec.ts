import { TestBed } from '@angular/core/testing';

import { TipoVisitaService } from './tipo-visita.service';

describe('TipoVisitaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipoVisitaService = TestBed.get(TipoVisitaService);
    expect(service).toBeTruthy();
  });
});
