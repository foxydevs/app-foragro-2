import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PlanificacionDetallePage } from './planificacion-detalle.page';

const routes: Routes = [
  {
    path: '',
    component: PlanificacionDetallePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PlanificacionDetallePage]
})
export class PlanificacionDetallePageModule {}
