import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanificacionDetallePage } from './planificacion-detalle.page';

describe('PlanificacionDetallePage', () => {
  let component: PlanificacionDetallePage;
  let fixture: ComponentFixture<PlanificacionDetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanificacionDetallePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanificacionDetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
