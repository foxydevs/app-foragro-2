import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { FormVisitaService } from 'src/app/_services/form-visita.service';
import { NetworkService } from 'src/app/_services/network.service';
import { ModalController, AlertController, ActionSheetController } from '@ionic/angular';
import { ModalVisitaComponent } from '../visita/modal-visita/modal-visita.component';
import { ModalPlanificacionComponent } from '../planificacion/modal-planificacion/modal-planificacion.component';

@Component({
  selector: 'app-planificacion-detalle',
  templateUrl: './planificacion-detalle.page.html',
  styleUrls: ['./planificacion-detalle.page.scss'],
})
export class PlanificacionDetallePage implements OnInit {
  private title:string = 'Planificación';
  private visitas:any[];
  private parameter:any;
  private disabledBtn:boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    private notificationService: NotificacionService,
    private networkService: NetworkService,
    private mainService: FormVisitaService,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.parameter = this.activatedRoute.snapshot.paramMap.get('id');
    if(this.networkService.isOnline()) {
      this.getAll(this.parameter)
    } else {
      this.notificationService.alertToast('La red ha sido desconectada D:');
    }
  }

  //SIGUIENTE
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  //ATRAS
  goToBack() {
    this.location.back();
  }

  //CARGAR
  public getAll(id:number){
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getAllClient(id)
    .subscribe((res) => {
      console.log(res)
      this.visitas = [];
      this.visitas = res;
      this.visitas.reverse();
      this.notificationService.dismiss();
    },(error) => {
      this.notificationService.dismiss();
      console.clear;
    })
  }

  async openModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalPlanificacionComponent,
      componentProps: { value: id, 
      cliente: this.parameter }
    });
    modal.onDidDismiss().then((data) => {
      if(data.data) {
        if(this.networkService.isOnline()) {
          this.getAll(this.parameter)
        } else {
          this.notificationService.alertToast('La red ha sido desconectada D:');
        }
      }      
    });
    return await modal.present();
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalVisitaComponent,
      componentProps: { value: id }
    });
    return await modal.present();
  }

  //ELIMINAR
  delete(id:any) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Formulario Eliminado', 'El formulario ha sido eliminado exitosamente.');
      this.getAll(this.parameter)
    },(error) => {
      this.notificationService.alertToast('Error al eliminar formulario.');
      console.error(error)
    });
  }

  //DELETE
  async confirmation(data:any) {
    if(this.networkService.isOnline()) {
    const alert = await this.alertController.create({
      header: 'Eliminar Formulario',
      message: '¿Desea eliminar el formulario?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'OK',
          handler: () => {
            this.delete(data);
          }
        }
      ]
    });
    await alert.present();
    } else {
      this.notificationService.alertToast('La red ha sido desconectada. Por favor, inténtelo más tarde.');
    }
  }

  //ACTION SHEET
  async presentActionSheet(id:number) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones de Formulario',
      buttons: [{
        text: 'Ver Detalles',
        icon: 'eye',
        handler: () => {
          this.presentModal(id);
        }
      }, {
        text: 'Eliminar',
        icon: 'trash',
        handler: () => {
          this.confirmation(id)
        }
      },
      {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

}
