import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, AlertController } from '@ionic/angular';
import { VentasService } from 'src/app/_services/ventas.service';
import { TipoVentaService } from 'src/app/_services/tipo-venta.service';
import { ClienteService } from 'src/app/_services/cliente.service';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { ModalProductosComponent } from '../modal-productos/modal-productos.component';

@Component({
  selector: 'app-modal-venta',
  templateUrl: './modal-venta.component.html',
  styleUrls: ['./modal-venta.component.scss'],
})
export class ModalVentaComponent implements OnInit {
  private title:string = '';
  data = {
    cliente: '',
    usuario: localStorage.getItem('currentId'),
    total: 0,
    fecha: new Date().toISOString(),
    tipo: '',
    tipoPlazo: '',
    plazo: '',
    nombres: '',
    nit: '',
    direccion: '',
    comprobante: 0,
    detalle: [],
    id: ''
  }
  cartProducts:any = [];
  disabledBtn:boolean;
  Ventas:any;
  selectedItem:any = 'form';
  
  constructor(
  public modalController: ModalController,
  public navParams: NavParams,
  public mainService: VentasService,
  public secondService: TipoVentaService,
  public thirdService: ClienteService,
  public notificationService: NotificacionService,
  public alertCtrl: AlertController
  ) { }

  ngOnInit() {
    this.data.id = this.navParams.get('value');
    this.data.cliente = this.navParams.get('cliente');
    this.getAll();
    this.getInvoice();
    this.getSingleCliente(this.data.cliente);
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    let fecha = (this.data.fecha.replace('T',' ').replace('Z', '')).split(" ");
    this.data.fecha = fecha[0];
    if(this.data.detalle) {
      if(this.data.tipo) {
        this.disabledBtn = true;
        this.create(this.data);
      } else {
        this.notificationService.alertToast('El tipo es requerido.');
      }
    } else {
      this.notificationService.alertToast('Los productos son requeridos.');
    }
    
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  //SEGMENT CHANGED
  segmentChanged(ev: any) {
    this.selectedItem = ev.detail.value;
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Venta Agregada', 'La venta fue agregada exitosamente.');
      this.closeModal();
    }, (error) => {
      this.disabledBtn = false;
      console.error(error)
    });
  }

  //CARGAR
  public getAll() {
    this.secondService.getAll()
    .subscribe((res) => {
      this.Ventas = res;
    }, (error) => {
      console.clear
    })
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalProductosComponent
    });
    modal.onDidDismiss().then((data) => {
      if(data.data) {
        this.cartProducts.push(data.data);
        this.sumaTotal();
      }
    });
    return await modal.present();
  }

  //GET SINGLE
  public getSingleCliente(id:any){
    this.thirdService.getSingle(id)
    .subscribe((res) => {
      this.data.cliente = res.id;
      this.data.nombres = res.nombre + ' ' + res.apellido;
      this.data.direccion = res.direccion;
      this.data.nit = res.nit;
    }, (error) => {
      console.error(error);
    })
  }

  //CARGAR FACTURA
  getInvoice() {
    this.mainService.getComprobante()
    .subscribe(res => {
      let x:number;
      x = +res.comprobante;
      this.data.comprobante = +res.comprobante + 1;
    }, (error) => {
      console.error(error)
    })
  }

  suma(x:number, y:number):number {
    let total = x + y;
    return total;
  }

  async deleteCar(e:any) {
    let confirm = await this.alertCtrl.create({
      header: '¿Desea eliminar el producto del carrito?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.cartProducts.splice(this.cartProducts.indexOf(e),1)
            this.sumaTotal();
          }
        }
      ]
    });
    await confirm.present();
  }

  addMore(e:any) {
    let cant = parseInt(e.cantidad) + 1;
    let sub = cant * e.precioVenta;
    sub.toFixed(2);
    let data = {
      id : e.id,
      codigo : e.codigo,
      nombre : e.nombre,
      descripcion : e.descripcion,
      marca : e.marcaDes,
      tipo : e.tipo,
      producto : e.id,
      precioVenta : e.precioVenta,
      cantidad : cant,
      precioClienteEs : e.precioClienteEs,
      precioDistribuidor : e.precioDistribuidor,
      subtotal : sub
    }
    for (var x in this.cartProducts) {
      if (this.cartProducts[x] == e) {
        this.cartProducts[x] = data;
        this.sumaTotal();
        break; //Stop this loop, we found it!
      }
    }
  }

  removeProduct(e:any) {
    let cant = parseInt(e.cantidad) - 1;
    let sub = cant * e.precioVenta;
    sub.toFixed(2);
    let data = {
      id : e.id,
      codigo : e.codigo,
      nombre : e.nombre,
      descripcion : e.descripcion,
      marca : e.marcaDes,
      tipo : e.tipo,
      producto : e.id,
      precioVenta : e.precioVenta,
      cantidad : cant,
      precioClienteEs : e.precioClienteEs,
      precioDistribuidor : e.precioDistribuidor,
      subtotal : sub
    }
    for (var x in this.cartProducts) {
      if (this.cartProducts[x] == e) {
        if(this.cartProducts[x].cantidad > 1) {
          this.cartProducts[x] = data;
          this.sumaTotal();
        }        
        break; //Stop this loop, we found it!
      }
    }
  }

  sumaTotal() {
    let a:number = 0;
    let b:number = 0;
    this.data.detalle = this.cartProducts;
    for(let x of this.cartProducts) {
      a = x.subtotal;
      b += a;
    }
    this.data.total = b;
  }

}
