import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VentaPage } from './venta.page';
import { ModalVentaComponent } from './modal-venta/modal-venta.component';
import { ModalProductosComponent } from './modal-productos/modal-productos.component';

const routes: Routes = [
  {
    path: '',
    component: VentaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    VentaPage,
    ModalVentaComponent,
    ModalProductosComponent,
  ],
  entryComponents: [
    ModalVentaComponent,
    ModalProductosComponent,
  ]
})
export class VentaPageModule {}
