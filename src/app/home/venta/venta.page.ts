import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { VentasService } from 'src/app/_services/ventas.service';
import { ModalVentaComponent } from './modal-venta/modal-venta.component';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { NetworkService } from 'src/app/_services/network.service';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.page.html',
  styleUrls: ['./venta.page.scss'],
})
export class VentaPage implements OnInit {
  private title:string = 'Ventas';
  private parameter:any;
  private sales:any = [];

  //CONSTRUCTOR
  constructor(
  private router:Router,
  private location:Location,
  private modalController: ModalController,
  private route: ActivatedRoute,
  private mainService: VentasService,
  private notificationService: NotificacionService,
  private networkService: NetworkService
  ) { }

  ngOnInit() {
    this.parameter = this.route.snapshot.paramMap.get('id');
    if(this.networkService.isOnline()) {
      this.getAll();
    } else {
      this.notificationService.alertToast('La red ha sido desconectada D:');
    }
  }

  //SIGUIENTE
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  //ATRAS
  goToBack() {
    this.location.back();
  }

  //OPEN MODAL
  async presentModal(id?:any) {
    const modal = await this.modalController.create({
      component: ModalVentaComponent,
      componentProps: { value: id, cliente: this.parameter }
    });
    return await modal.present();
  }

  //CARGAR
  public getAll() {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getAll()
    .subscribe(res => {
      this.sales = [];
      this.sales = res;
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();
      console.error(error)
    })
  }

}
