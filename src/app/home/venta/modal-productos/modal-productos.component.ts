import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/_services/producto.service';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { ModalController } from '@ionic/angular';
import { TipoProductoService } from 'src/app/_services/tipo-producto.service';

@Component({
  selector: 'app-modal-productos',
  templateUrl: './modal-productos.component.html',
  styleUrls: ['./modal-productos.component.scss'],
})
export class ModalProductosComponent implements OnInit {
  private title:string = 'Productos';
  private products:any;
  private typesProducts:any[];
  private select:boolean = true;
  private form:boolean = false;
  private precioClienteEs:boolean = false;
  private precioDistribuidor:boolean = false;
  product = {
    id: '',
    codigo: '',
    nombre: '',
    descripcion: '',
    marca: '',
    tipo: '',
    tipos: '',
    producto: '',
    precioVenta: 0,
    cantidad: 0,
    precioClienteEs: '',
    precioDistribuidor: '',
    subtotal: 0
  }

  //CONSTRUCTOR
  constructor(private mainService: ProductoService,
    private secondService: TipoProductoService,
    private notificationService: NotificacionService,
    private modalController: ModalController) { }

  ngOnInit() {
    this.getAllTypes();
    this.getAll();
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }
  
  //CARGAR
  public getAll() {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getAllExistencia()
    .subscribe((res) => {
      this.notificationService.dismiss();
      this.products = res;
    }, (error) => {
      this.notificationService.dismiss();
      console.error(error)
    })
  }

  public selectProduct(data:any) {
    this.select = false;
    this.form = true;
    this.product.id = data.id
    this.product.codigo = data.productos.codigo
    this.product.nombre = data.productos.nombre
    this.product.descripcion = data.productos.descripcion
    this.product.marca = data.productos.marcaDes
    this.product.tipo = data.productos.tipo
    this.product.tipos = data.productos.tipos.descripcion
    this.product.producto = data.productos.id
    this.product.precioVenta = data.precioVenta
    this.product.cantidad = 0
    this.product.precioClienteEs = data.precioClienteEs
    this.product.precioDistribuidor = data.precioDistribuidor
    this.precioClienteEs = true;
    this.precioDistribuidor = true;
  }

  public saveChanges() {
    this.product.subtotal = this.product.cantidad * this.product.precioVenta;
    this.modalController.dismiss(this.product);
  }

  //CARGAR
  public getAllTypes() {
    this.secondService.getAll()
    .subscribe((res) => {
      this.typesProducts = [];
      this.typesProducts = res;
    }, (error) => {
      console.log(error)
    })
  }

}
