import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController, AlertController } from '@ionic/angular';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { ClienteService } from 'src/app/_services/cliente.service';
import { FormVisitaService } from 'src/app/_services/form-visita.service';
import { NetworkService } from 'src/app/_services/network.service';

@Component({
  selector: 'app-offline',
  templateUrl: './offline.page.html',
  styleUrls: ['./offline.page.scss'],
})
export class OfflinePage implements OnInit {
  private title:string = 'Data Offline';
  private Table:any[];
  private Table2:any[];
  private selectedItem:any = 'clientes';

  constructor(private router:Router,
  private location:Location,
  private notificationService: NotificacionService,
  private networkService: NetworkService,
  private alertController: AlertController,
  private mainService: ClienteService,
  private secondService: FormVisitaService,
  ) { }

  ngOnInit() {
    this.getAll();
  }

  //SIGUIENTE
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  //REGRESAR
  goToBack() {
    this.location.back();
  }

  //GET
  getAll() {
    this.Table = JSON.parse(localStorage.getItem('currentClientOffline'))
    this.Table2 = JSON.parse(localStorage.getItem('currentVisitaOffline'))
  }

  //AGREGAR
  create(formValue:any) {
    if(this.networkService.isOnline()) {
      this.mainService.create(formValue)
      .subscribe((res) => {
        this.Table.splice(this.Table.indexOf(formValue),1)
        localStorage.removeItem('currentClientOffline');
        localStorage.setItem('currentClientOffline', JSON.stringify(this.Table))
        this.notificationService.alertMessage('Cliente Agregado', 'El cliente fue agregado exitosamente.');
      }, (error) => {
        console.error(error)
      });
    } else {
      this.notificationService.alertToast('La red ha sido desconectada. Por favor, inténtelo más tarde.');      
    }    
  }

  //AGREGAR
  create2(formValue:any) {
    if(this.networkService.isOnline()) {
      this.secondService.create(formValue)
      .subscribe((res) => {
        this.Table2.splice(this.Table2.indexOf(formValue),1)
        localStorage.removeItem('currentVisitaOffline');
        localStorage.setItem('currentVisitaOffline', JSON.stringify(this.Table2))
        this.notificationService.alertMessage('Formulario Agregado', 'El formulario fue agregado exitosamente.');
      }, (error) => {
        console.error(error)
      });
    } else {
      this.notificationService.alertToast('La red ha sido desconectada. Por favor, inténtelo más tarde.');      
    }
  }

  async deleted(e:any) {
    const alert = await this.alertController.create({
      header: 'Confirmar Eliminación',
      message: '¿Desea eliminar la información almacenada previamente?, No se podra recuperar la información almacenada.',
      buttons: [{
        text: 'Cancelar',
        handler: () => {

        }
      },{
        text: 'Aceptar',
        handler: () => {
          this.Table.splice(this.Table.indexOf(e),1)
          localStorage.removeItem('currentClientOffline');
          localStorage.setItem('currentClientOffline', JSON.stringify(this.Table))
        }
      }]
    });
    await alert.present();
  }

  async deleted2(e:any) {
    const alert = await this.alertController.create({
      header: 'Confirmar Eliminación',
      message: '¿Desea eliminar la información almacenada previamente?, No se podra recuperar la información almacenada.',
      buttons: [{
        text: 'Cancelar',
        handler: () => {

        }
      },{
        text: 'Aceptar',
        handler: () => {
          this.Table2.splice(this.Table2.indexOf(e),1)
          localStorage.removeItem('currentVisitaOffline');
          localStorage.setItem('currentVisitaOffline', JSON.stringify(this.Table2))
        }
      }]
    });
    await alert.present();
  }

  //SEGMENT CHANGED
  segmentChanged(ev: any) {
    this.selectedItem = ev.detail.value;
  }

}
