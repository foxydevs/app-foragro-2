import { Component, OnInit } from '@angular/core';
import { FormVisitaService } from 'src/app/_services/form-visita.service';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { NavParams, ModalController } from '@ionic/angular';
import { NetworkService } from 'src/app/_services/network.service';

@Component({
  selector: 'app-modal-visita',
  templateUrl: './modal-visita.component.html',
  styleUrls: ['./modal-visita.component.scss'],
})
export class ModalVisitaComponent implements OnInit {
  //PROPIEDADES
  private data:any;
  private parameter:any;
  private selectedItem:any = 'plan'

  constructor(
    private mainService: FormVisitaService,
    private networkService: NetworkService,
    private notificationService: NotificacionService,
    private navParams: NavParams,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value');
    if(this.networkService.isOnline()) {
      this.getSingleCliente(this.parameter)
    } else {
      this.notificationService.alertToast('La red ha sido desconectada. Por favor, inténtelo más tarde.');
    }    
  }

  //GET SINGLE
  getSingleCliente(id:any){
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getSingle(id)
    .subscribe((res) => {
      this.data = [];
      this.data = res;
      this.notificationService.dismiss();
    },(error) => {
      this.notificationService.dismiss();
      console.error(error);
    })
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  //SEGMENT CHANGED
  segmentChanged(ev: any) {
    this.selectedItem = ev.detail.value;
  }
}
