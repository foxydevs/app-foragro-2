import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { LoadingController, Platform, ActionSheetController, ModalController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { ClienteService } from 'src/app/_services/cliente.service';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { NetworkService } from 'src/app/_services/network.service';
import { ClienteUsuarioService } from 'src/app/_services/cliente-usuario.service';
import { FormVisitaService } from 'src/app/_services/form-visita.service';

//GOOGLE
declare var google;

@Component({
  selector: 'app-visita',
  templateUrl: './visita.page.html',
  styleUrls: ['./visita.page.scss'],
})
export class VisitaPage implements OnInit {
  private title:string = 'Visitas';
  table: any;
  selectedData: any;
  private itemSelected:string = 'visitar';
  idUserApp:any = localStorage.getItem('currentId');

  //PROPIEDADES GOOGLE MAPS
  map: any;
  directionsService: any = null;
  directionsDisplay: any = null;
  bounds: any = null;
  myLatLng: any;
  waypoints: any[];
  clientes: any[];
  visitas: any[];
  marker:any;
  labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  labelIndex = 0;
  latitude = 0;
  longitude = 0;
  buttonSendMail:boolean;
  btnRoute:boolean;
  dataInit = {
    id_usuario: localStorage.getItem('currentId'),
  	fecha: '',
  	lon_inicial: 0,
  	lat_inicial: 0,
  	estado: ''
  }
  dataEnd = {
    id: localStorage.getItem('currentRouteID'),
  	lon_final: 0,
  	lat_final: 0,
  	estado: ''
  }

  constructor(
    public router: Router,
    public location:Location,
    public loadingController: LoadingController,
    public mainService: ClienteService,
    public modalController: ModalController,
    public geolocation: Geolocation,
    public notificationService: NotificacionService,
    public platform: Platform,
    public actionSheetController: ActionSheetController,
    public launchNavigator: LaunchNavigator,
    public networkService: NetworkService,
    private secondService: ClienteUsuarioService,
    private thirdService: FormVisitaService,
  ) { }

  ngOnInit() {
    this.notificationService.alertLoading('Cargando...', 15000);
  }

  //SIGUIENTE
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  //ANTERIOR
  goToBack() {
    this.location.back();
  }

  //INICIAR RUTA
  async initRoute() {
    localStorage.setItem('currentRoute', 'true');
    this.btnRoute = true;
    this.dataInit.fecha = new Date().toISOString().split('T')[0];
    this.dataInit.lat_inicial = this.latitude;
    this.dataInit.lon_inicial = this.longitude;
    this.dataInit.estado = '0';
    console.log(this.dataInit);
    this.startRoute(this.dataInit);
  }

  //FINALIZAR RUTA
  endRoute() {
    localStorage.setItem('currentRoute', 'false');
    this.btnRoute = false;
    this.dataEnd.id = localStorage.getItem('currentRouteID')
    this.dataEnd.lat_final = this.latitude;
    this.dataEnd.lon_final = this.longitude;
    this.dataEnd.estado = '1';

    console.log(this.dataEnd);
    this.finalizeRoute(this.dataEnd);
  }

  //GO DETALLE CLIENTE
  goClient(id:any, usuarioCliente:string) {
    this.goToRoute('home/visita-detalle/' + id);
    localStorage.setItem('currentUsuarioCliente', usuarioCliente);    
  }

  //SEGMENT CHANGED
  segmentChanged(ev: any) {
    this.itemSelected = ev.detail.value;
    if(this.itemSelected == 'visitar') {
      if(!this.networkService.isOnline()) {
        this.notificationService.alertToast('La red ha sido desconectada. El mapa no puede visualizarse.');
      }
    }    
  }

  async getLocation(latitude:any, longitude:any, data?:any) {
    const actionSheet = await this.actionSheetController.create({
      header: 'App de Ubicación',
      buttons: [
      {
        text: 'Google Maps',
        icon: 'map',
        handler: () => {
          let options: LaunchNavigatorOptions = {
            start: `${this.latitude},${this.longitude}`,
            app: this.launchNavigator.APP.GOOGLE_MAPS
          }
          
          this.launchNavigator.navigate([latitude, longitude], options)
          .then(
            success => console.log('Launched navigator'),
            error => console.log('Error launching navigator', error)
          );
        }
      }, {
        text: 'Waze',
        icon: 'pin',
        handler: () => {
          let options: LaunchNavigatorOptions = {
            start: `${this.latitude},${this.longitude}`,
            app: this.launchNavigator.APP.WAZE
          }
          
          this.launchNavigator.navigate([latitude, longitude], options)
          .then(
            success => console.log('Launched navigator'),
            error => console.log('Error launching navigator', error)
          );
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  getPosition():any{
    this.geolocation.getCurrentPosition()
    .then(resp => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      this.getAll(this.idUserApp, this.latitude, this.longitude);
      this.getAllClients(this.idUserApp, this.latitude, this.longitude);
     }).catch(error => {
      console.log('No location.');
    });
  }

  //CARGAR
  public getAll(id:any, lat:any, lng:any) {
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    this.bounds = new google.maps.LatLngBounds();
    this.mainService.getNear(id, lat, lng)
    .subscribe((res) => {
      this.waypoints = [];
      res.forEach((x) => {
        if(x.latitud && x.estado == '1') {
          let data2 = {
            location: {lat: parseFloat(x.latitud), lng: parseFloat(x.longitud)},
            stopover: true,
          }
          this.waypoints.push(data2)
        }
      });
      this.notificationService.dismiss();
    },(error) => {
      console.log('No location.');
      this.notificationService.dismiss();
    })
  }

  //CARGAR
  public getAllClients(id:any, lat:any, lng:any) {
    if(this.networkService.isOnline()) {
      this.mainService.getNear(id, lat, lng)
      .subscribe((res) => {
        this.clientes = [];
        res.forEach(x => {
          let data = {
            nombre: x.nombre,
            apellido: x.apellido,
            direccion: x.direccion,
            telefono: x.telefono,
            estado: x.estado,
            latitud: x.latitud, 
            longitud: x.longitud,
            distance: parseFloat(x.distance).toFixed(2) + ' K',
            id: x.id,
            usuarioidDelete: x.usuarioidDelete
          }
          this.clientes.push(data)
        })
        this.getMap();
        localStorage.setItem('currentClientes', JSON.stringify(this.clientes));
        this.notificationService.dismiss();
      },(error) => {
        this.notificationService.dismiss();
        console.log('No location.');
      })
    } else {
      this.clientes = JSON.parse(localStorage.getItem('currentClientes'));     
    }
  }

  async presentLoading(msg:string) {
    const loading = await this.loadingController.create({
      message: msg
    });
    return await loading.present();
  }
  
  getMap() {
    // create a new map by passing HTMLElement
    let mapEle: HTMLElement = document.getElementById('map');
  
    // create LatLng object
    this.myLatLng = {lat: this.latitude, lng: this.longitude};
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: this.myLatLng,
      zoom: 17
    });
  
    this.directionsDisplay.setMap(this.map);
  
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      mapEle.classList.add('show-map');
      this.calculateRoute();
    });
  }

  calculateRoute():void {
    if(this.waypoints.length > 0) {
      this.bounds.extend(this.myLatLng);
  
      this.waypoints.forEach(waypoint => {
        var point = new google.maps.LatLng(waypoint.lat, waypoint.lng);
        this.bounds.extend(point);
      });
    
      this.map.fitBounds(this.bounds);
    
      this.directionsService.route({
        origin: new google.maps.LatLng(this.latitude, this.longitude),
        destination: new google.maps.LatLng(this.waypoints[this.waypoints.length -1].location.lat, this.waypoints[this.waypoints.length -1].location.lng),
        //destination: new google.maps.LatLng(this.clientes[this.clientes.length -1].location.lat, this.clientes[this.clientes.length -1].location.lng),
        waypoints: this.waypoints,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING,
        avoidTolls: true
      }, (res, status)=> {
        if(status === google.maps.DirectionsStatus.OK) {
          this.directionsDisplay.setDirections(res);
        }else{
          alert('Could not display directions due to: ' + status);
        }
      });  
    } else {
      this.notificationService.alertToast('No existen datos cargados.');
    }  
  }

  //DO REFRESH
  doRefresh(event) {
    setTimeout(() => {
      if(this.networkService.isOnline()) {
        this.getPosition();
      } else {
        this.notificationService.alertToast('La red ha sido desconectada. El mapa no puede visualizarse.');
      }
      event.target.complete();
    }, 2000);
  }

  ionViewWillEnter() {
    this.getPosition();
    //BOTON SEMANA
    if(localStorage.getItem('currentStart') == 'true') {
      this.buttonSendMail = true;
    } else {
      this.buttonSendMail = false;
    }
    if(localStorage.getItem('currentRoute') == 'true') {
      this.btnRoute = true;
    } else {
      this.btnRoute = false;
    }
  }

  //FIN SEMANA
  async endWeek() {
    for(let x of this.clientes) {
      if(x.estado=='1') {
        await this.changeState(x.usuarioidDelete, 3);
      }
    }
    this.getPosition();
    localStorage.setItem('currentStart', 'false');
    this.buttonSendMail = false;
  }

  sendMail() {
    /*var d = new Date();
    let fechaFin = d.getFullYear() + '-' + (d.getMonth() +1) + '-' + d.getDate();
    console.log(fechaFin)
    var d2 = new Date();
    d2 = this.sumarDias(d2, -7);
    let fechaInicio = d2.getFullYear() + '-' + (d2.getMonth() +1) + '-' + d2.getDate();
    console.log(fechaInicio)
    this.thirdService.sendMail(this.idUserApp, fechaInicio, fechaFin, 1)
    .subscribe((res) => {
      this.notificationService.alertMessage('Planificación Enviada', 'La planificación ha sido enviada.')
    }, (error) => {
      console.error(error);
    });*/
  }

  sumarDias(fecha, dias){
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

  //CAMBIAR ESTADO
  changeState(id:any, state:any) {
    let data = {
      id: id,
      estado: state,
    }
    this.secondService.update(data)
    .subscribe((res) => {
    }, (error) => {
      console.clear
    })
  }

  startRoute(formValue:any) {
    this.thirdService.initRoute(formValue)
    .subscribe((res) => {
      console.log(res);
      localStorage.setItem('currentRouteID', res.id);
    },(error) => {
      console.error(error)
    });
  }

  finalizeRoute(formValue:any) {
    this.thirdService.endRoute(formValue)
    .subscribe((res) => {
      console.log(res);
      localStorage.removeItem('currentRouteID');
    },(error) => {
      console.error(error)
    });
  }

}
