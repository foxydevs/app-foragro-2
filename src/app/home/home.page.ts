import { Component, OnInit } from '@angular/core';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { PaisService } from '../_services/pais.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { UbicacionService } from '../_services/ubicacion.service';
import { NotificacionService } from '../_services/notificacion.service';
import { ModalController, AlertController } from '@ionic/angular';
import { TipoAvanceService } from '../_services/tipo-avance.service';
import { ObjetivoService } from '../_services/objetivo.service';
import { CultivoService } from '../_services/cultivo.service';
import { TipoVisitaService } from '../_services/tipo-visita.service';
import { SectorService } from '../_services/sector.service';
import { TipoClienteService } from '../_services/tipo-cliente.service';
import { NetworkService } from '../_services/network.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  //PROPIEDADES
  pictureHome:string = 'https://jovenessaa.com.ar/wp-content/uploads/2016/05/sembrar.jpg';
  pictureLogin:string = 'https://cdn.civitatis.com/costa-rica/arenal/galeria/siembra-pino.jpg';
  pictureLogin2:string = 'https://i.pinimg.com/originals/39/6b/f8/396bf8d3b496f5e9ab5347c2a5b14bbf.jpg';
  private codeCountry:string;
  private camiones: any[];
  private btnCamion:boolean;

  //CONSTRUCTOR
  constructor(private router: Router,
    private location: Location,
    private backgroundGeolocation: BackgroundGeolocation,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private mainService: PaisService,
    private secondService: UbicacionService,
    private thirdService: TipoAvanceService,
    private fourthService: ObjetivoService,
    private fifthService: CultivoService,
    private sixthService: TipoVisitaService,
    private eigthService: SectorService,
    private ninethService: TipoClienteService,
    private networkService: NetworkService,
    private notificationService: NotificacionService,
    private modalController: ModalController,
    private alertController: AlertController
    ) {}

  ngOnInit() {
    if(localStorage.getItem('currentState') == '21') {
      this.presentModal(2);
    }
    this.startBackgroundGeolocation();
    this.getPosition();
    this.getTrucks();
  }

  //GET POSITION
  getPosition() {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.geolocation.getCurrentPosition().then((resp) => {
      this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        let data = {
          nombre: result[0].countryName,
          codigo: result[0].countryCode,
          countryCode: result[0].countryCode,
          countryName: result[0].countryName,
          postalCode: result[0].postalCode,
          administrativeArea: result[0].administrativeArea,
          subAdministrativeArea: result[0].subAdministrativeArea,
          locality: result[0].locality,
        }
        this.getFilter(result[0].countryCode, data);
        this.codeCountry = result[0].countryCode;
      }).catch((error: any) => {
        console.error(error)
      });
    }).catch((error) => {
      console.error('Error getting location', error);
    });
  }

  //SIGUIENTE
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  //ANTERIOR
  goToBack() {
    this.location.back();
  }

  //PRESENT MODAL
  async presentModal(id?:number) {
    const modal = await this.modalController.create({
      component: 'ModalPerfilComponent',
      componentProps: { value: id }
    });
    modal.onDidDismiss().then((data) => {
      //this.getAll();
    });
    return await modal.present();
  }

  //GET TRUCK
  getTrucks() {
    this.camiones = JSON.parse(localStorage.getItem('currentCamiones'));
    if(this.camiones.length == 0) {
      this.btnCamion = true;
    } else if(this.camiones.length > 0) {
      this.btnCamion = false;
    }
  }

  //OPEN TRUCK
  openTruck() {
    if(this.camiones.length > 0) {
      this.goToRoute('home/visita')
    }
    /*if(this.camiones.length == 1) {
      this.goToRoute('home/camion/' + this.camiones[0].id);
    } else if(this.camiones.length > 1) {
      this.goToRoute('home/camiones');
    }*/
  }

  //START BACKGROUND LOCATION
  startBackgroundGeolocation(){
    this.backgroundGeolocation.isLocationEnabled()
    .then((res) =>{
      if(res){
        this.start();
      } else {
        this.backgroundGeolocation.showLocationSettings();
      }
    }).catch((error) =>{
      console.error(error)
    })
  }
  
  //START 
  start(){
    const config: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 100,
      distanceFilter: 1000,
      stopOnTerminate: false,
      //ANDROID ONLY SELECTION
      locationProvider: 1,
      startForeground: false,
      interval: 1800000,
      activitiesInterval: 1800000,
      fastestInterval: 1800000,
      stopOnStillActivity: false,
      debug: false,
      notificationTitle: "Foragro",
      notificationText: "",
    };
  
    console.log('start');
    let id = +localStorage.getItem('currentId');
    this.backgroundGeolocation
    .configure(config)
    .then((location: BackgroundGeolocationResponse ) => {
      console.log(location);
      let data = {
        latitude: location.latitude,
        longitude: location.longitude,
        direccion: '1 KM, 100 radius',
        usuario: id,
        estado: 1,
        tipo: 1
      }
      this.create(data);
    }).catch((error) => {
      console.error(error);
    });
  
    this.backgroundGeolocation.start();
  }

  //STROP BACKGROUND GEOLOCATION
  stopBackgroundGeolocation(){
    this.backgroundGeolocation.stop();
  }

  //GET FILTER
  public getFilter(data:any, form:any) {
    this.mainService.getFilter(data)
    .subscribe((res) => {
      localStorage.setItem('currentPais', res.id);
      if(res.length == 0) {
        this.createCountry(form);
      }
    }, (error) => {
      localStorage.setItem('currentPais', '');
      console.error(error);
    });
  }

  //AGREGAR
  createCountry(formValue:any) {
    this.mainService.create(formValue)
    .subscribe((res) => {
      localStorage.setItem('currentPais', res.id);
    }, (error) => {
      localStorage.setItem('currentPais', '');
      console.error(error);
    });
  }

  //AGREGAR
  create(formValue:any) {
    this.secondService.create(formValue)
    .subscribe((res) => {
      console.log(res);
    }, (error) => {
      /*if(error) {
        this.notificationService.alertToast('Ha ocurrido un error.');
      }*/
    });
  }

  //CERRAR SESION
  async logOut() {
    if(JSON.parse(localStorage.getItem('currentClientOffline')) ||
    JSON.parse(localStorage.getItem('currentVisitaOffline')) || 
    JSON.parse(localStorage.getItem('currentVisitaDiarioOffline'))) {
      if(JSON.parse(localStorage.getItem('currentClientOffline')).length > 0 ||
      JSON.parse(localStorage.getItem('currentVisitaOffline')).length  > 0 || 
      JSON.parse(localStorage.getItem('currentVisitaDiarioOffline')).length  > 0) {
        const alert = await this.alertController.create({
          header: 'Cerrar Sesión',
          message: '¿Desea cerrar sesión?, Hay información almacenada sin conexión, si cierra sesión no podra recuperar la información almacenada.',
          buttons: [{
            text: 'Cancelar',
            handler: () => {
    
            }
          },{
            text: 'Aceptar',
            handler: () => {
              localStorage.clear();
              this.goToRoute('login')
            }
          }]
        });
        alert.present();
      }
    } else {
      localStorage.clear();
      this.goToRoute('login')
    }    
  }

  //GET ALL SERVICES
  getAll() {
    if(this.networkService.isOnline()) {
      this.getAllThird();
      this.getAllFourth();
      this.getAllFifth();
      this.getAllSixth();
      this.getAllEigth();
      this.getAllNineth();
    }    
  }

  public getAllThird(){
    this.thirdService.getAll()
    .subscribe((res) => {
      localStorage.setItem('currentTipoAvance', JSON.stringify(res));
    },(error) => {
      console.clear;
    })
  }

  
  public getAllFourth(){
    this.fourthService.getAll()
    .subscribe((res) => {
      localStorage.setItem('currentTipoObjetivos', JSON.stringify(res));
    },(error) => {
      console.clear;
    })
  }

  public getAllFifth(){
    this.fifthService.getAll()
    .subscribe((res) => {
      localStorage.setItem('currentTipoCultivo', JSON.stringify(res));
    }, (error) => {
      console.clear;
    })
  }

  public getAllSixth(){
    this.sixthService.getAll()
    .subscribe((res) => {
      localStorage.setItem('currentTipoVisita', JSON.stringify(res));
    },(error) => {
      console.clear;
    })
  }

  public getAllSeventh(){
    this.mainService.getAll()
    .subscribe((res) => {
      localStorage.setItem('currentPaises', JSON.stringify(res));
    },(error) => {
      console.clear;
    })
  }

  public getAllEigth(){
    this.eigthService.getAll()
    .subscribe((res) => {
      localStorage.setItem('currentSectores', JSON.stringify(res));
    }, (error) => {
      console.clear;
    })
  }

  public getAllNineth(){
    this.ninethService.getAll()
    .subscribe((res) => {
      localStorage.setItem('currentTipoCliente', JSON.stringify(res));
    },(error) => {
      console.clear;
    })
  }

  //SLIDE CUBE
  slidesOpts = {
    grabCursor: true,
    cubeEffect: {
      shadow: true,
      slideShadows: true,
      shadowOffset: 20,
      shadowScale: 0.94,
    },
    on: {
      beforeInit: function() {
        const swiper = this;
        swiper.classNames.push(`${swiper.params.containerModifierClass}cube`);
        swiper.classNames.push(`${swiper.params.containerModifierClass}3d`);
  
        const overwriteParams = {
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerGroup: 1,
          watchSlidesProgress: true,
          resistanceRatio: 0,
          spaceBetween: 0,
          centeredSlides: false,
          virtualTranslate: true,
        };
  
        this.params = Object.assign(this.params, overwriteParams);
        this.originalParams = Object.assign(this.originalParams, overwriteParams);
      },
      setTranslate: function() {
        const swiper = this;
        const {
          $el, $wrapperEl, slides, width: swiperWidth, height: swiperHeight, rtlTranslate: rtl, size: swiperSize,
        } = swiper;
        const params = swiper.params.cubeEffect;
        const isHorizontal = swiper.isHorizontal();
        const isVirtual = swiper.virtual && swiper.params.virtual.enabled;
        let wrapperRotate = 0;
        let $cubeShadowEl;
        if (params.shadow) {
          if (isHorizontal) {
            $cubeShadowEl = $wrapperEl.find('.swiper-cube-shadow');
            if ($cubeShadowEl.length === 0) {
              $cubeShadowEl = swiper.$('<div class="swiper-cube-shadow"></div>');
              $wrapperEl.append($cubeShadowEl);
            }
            $cubeShadowEl.css({ height: `${swiperWidth}px` });
          } else {
            $cubeShadowEl = $el.find('.swiper-cube-shadow');
            if ($cubeShadowEl.length === 0) {
              $cubeShadowEl = swiper.$('<div class="swiper-cube-shadow"></div>');
              $el.append($cubeShadowEl);
            }
          }
        }
  
        for (let i = 0; i < slides.length; i += 1) {
          const $slideEl = slides.eq(i);
          let slideIndex = i;
          if (isVirtual) {
            slideIndex = parseInt($slideEl.attr('data-swiper-slide-index'), 10);
          }
          let slideAngle = slideIndex * 90;
          let round = Math.floor(slideAngle / 360);
          if (rtl) {
            slideAngle = -slideAngle;
            round = Math.floor(-slideAngle / 360);
          }
          const progress = Math.max(Math.min($slideEl[0].progress, 1), -1);
          let tx = 0;
          let ty = 0;
          let tz = 0;
          if (slideIndex % 4 === 0) {
            tx = -round * 4 * swiperSize;
            tz = 0;
          } else if ((slideIndex - 1) % 4 === 0) {
            tx = 0;
            tz = -round * 4 * swiperSize;
          } else if ((slideIndex - 2) % 4 === 0) {
            tx = swiperSize + (round * 4 * swiperSize);
            tz = swiperSize;
          } else if ((slideIndex - 3) % 4 === 0) {
            tx = -swiperSize;
            tz = (3 * swiperSize) + (swiperSize * 4 * round);
          }
          if (rtl) {
            tx = -tx;
          }
  
           if (!isHorizontal) {
            ty = tx;
            tx = 0;
          }
  
           const transform$$1 = `rotateX(${isHorizontal ? 0 : -slideAngle}deg) rotateY(${isHorizontal ? slideAngle : 0}deg) translate3d(${tx}px, ${ty}px, ${tz}px)`;
          if (progress <= 1 && progress > -1) {
            wrapperRotate = (slideIndex * 90) + (progress * 90);
            if (rtl) wrapperRotate = (-slideIndex * 90) - (progress * 90);
          }
          $slideEl.transform(transform$$1);
          if (params.slideShadows) {
            // Set shadows
            let shadowBefore = isHorizontal ? $slideEl.find('.swiper-slide-shadow-left') : $slideEl.find('.swiper-slide-shadow-top');
            let shadowAfter = isHorizontal ? $slideEl.find('.swiper-slide-shadow-right') : $slideEl.find('.swiper-slide-shadow-bottom');
            if (shadowBefore.length === 0) {
              shadowBefore = swiper.$(`<div class="swiper-slide-shadow-${isHorizontal ? 'left' : 'top'}"></div>`);
              $slideEl.append(shadowBefore);
            }
            if (shadowAfter.length === 0) {
              shadowAfter = swiper.$(`<div class="swiper-slide-shadow-${isHorizontal ? 'right' : 'bottom'}"></div>`);
              $slideEl.append(shadowAfter);
            }
            if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
            if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
          }
        }
        $wrapperEl.css({
          '-webkit-transform-origin': `50% 50% -${swiperSize / 2}px`,
          '-moz-transform-origin': `50% 50% -${swiperSize / 2}px`,
          '-ms-transform-origin': `50% 50% -${swiperSize / 2}px`,
          'transform-origin': `50% 50% -${swiperSize / 2}px`,
        });
  
         if (params.shadow) {
          if (isHorizontal) {
            $cubeShadowEl.transform(`translate3d(0px, ${(swiperWidth / 2) + params.shadowOffset}px, ${-swiperWidth / 2}px) rotateX(90deg) rotateZ(0deg) scale(${params.shadowScale})`);
          } else {
            const shadowAngle = Math.abs(wrapperRotate) - (Math.floor(Math.abs(wrapperRotate) / 90) * 90);
            const multiplier = 1.5 - (
              (Math.sin((shadowAngle * 2 * Math.PI) / 360) / 2)
              + (Math.cos((shadowAngle * 2 * Math.PI) / 360) / 2)
            );
            const scale1 = params.shadowScale;
            const scale2 = params.shadowScale / multiplier;
            const offset$$1 = params.shadowOffset;
            $cubeShadowEl.transform(`scale3d(${scale1}, 1, ${scale2}) translate3d(0px, ${(swiperHeight / 2) + offset$$1}px, ${-swiperHeight / 2 / scale2}px) rotateX(-90deg)`);
          }
        }
  
        const zFactor = (swiper.browser.isSafari || swiper.browser.isUiWebView) ? (-swiperSize / 2) : 0;
        $wrapperEl
          .transform(`translate3d(0px,0,${zFactor}px) rotateX(${swiper.isHorizontal() ? 0 : wrapperRotate}deg) rotateY(${swiper.isHorizontal() ? -wrapperRotate : 0}deg)`);
      },
      setTransition: function(duration) {
        const swiper = this;
        const { $el, slides } = swiper;
        slides
          .transition(duration)
          .find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left')
          .transition(duration);
        if (swiper.params.cubeEffect.shadow && !swiper.isHorizontal()) {
          $el.find('.swiper-cube-shadow').transition(duration);
        }
      },
    }
  }
}
