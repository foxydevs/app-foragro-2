import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ClienteService } from 'src/app/_services/cliente.service';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { NetworkService } from 'src/app/_services/network.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

//GOOGLE
declare var google;

@Component({
  selector: 'app-modal-cliente',
  templateUrl: './modal-cliente.component.html',
  styleUrls: ['./modal-cliente.component.scss'],
})
export class ModalClienteComponent implements OnInit {
  private title:string = 'Formulario Cliente';
  private parameter:any;
  private sectores:any[] = [];
  private map: any;
  private id:number = +localStorage.getItem('currentId');
  private disabledBtn:boolean = false;
  data = {
    nombre: '',
    municipio: '',
    departamento: '',
    direccion: '',
    telefono: '',
    sector: '',
    longitud: 0,
    nit: 1234567,
    latitud: 0,
    pais: localStorage.getItem('currentPais'),
    usuario: this.id,
    id: 0,
    estado: 2
  }

  //CONSTRUCTOR
  constructor(private modalController: ModalController,
    private navParams: NavParams,
    private mainService: ClienteService,
    private networkService: NetworkService,
    private notificationService: NotificacionService,
    private geolocation: Geolocation,
  ) {
    this.notificationService.alertLoading('Cargando...', 15000);
    //this.getPosition();
  }

  ngOnInit() {
    this.sectores = JSON.parse(localStorage.getItem('currentSectores'));
    this.parameter = this.navParams.get('value');
    if(this.parameter) {
      this.getSingle(this.parameter);
    } else {
      this.getPosition();
      if(!this.networkService.isOnline()) {
        this.notificationService.alertToast('La red ha sido desconectada. El mapa no puede visualizarse.');
      }
    }

    
  }

  //SAVE CHANGES
  saveChanges() {
    this.data.usuario = +localStorage.getItem('currentId');
    if(this.data.nombre) {
      if(this.parameter) {
        if(this.networkService.isOnline()) {
          this.updated(this.data);
        } else {
          this.notificationService.alertToast('La red ha sido desconectada. Por favor, inténtelo más tarde');
        }
      } else {
        if(this.networkService.isOnline()) {
          this.create(this.data);
        } else {
          this.notificationService.alertToast('La red ha sido desconectada.');
          this.disabledBtn = true;
          this.createOffline(this.data);
          this.notificationService.alertMessage('Sin Conexión', 'El cliente fue agregado en la sección SIN CONEXIÓN.');
          this.closeModal();
        }
      }
    } else {
      this.notificationService.alertToast('El nombre es requerido.');
    }    
  }

  //GET SINGLE
  public getSingle(id:any){
    this.mainService.getSingle(id)
    .subscribe(res => {
      this.data = res;
      this.loadMapUpdate(res.latitud, res.longitud);
      if(!this.networkService.isOnline()) {
        this.notificationService.alertToast('La red ha sido desconectada. El mapa no puede visualizarse.');
      }
      this.notificationService.dismiss();
    },(error) => {
      this.notificationService.dismiss();
      console.error(error);
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.disabledBtn = true;
    this.mainService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Cliente Agregado', 'El cliente fue agregada exitosamente.');
      this.closeModal(res);
    },(error) => {
      this.disabledBtn = false;
      console.log(error)
    });
  }

  //AGREGAR
  createOffline(data:any) {
    let client:any[] = []
    if(localStorage.getItem('currentClientOffline')) {
      client = JSON.parse(localStorage.getItem('currentClientOffline'))
    }
    client.push(data)
    localStorage.removeItem('currentClientOffline')
    localStorage.setItem('currentClientOffline', JSON.stringify(client));
  }

  //ACTUALIZAR
  updated(formValue:any) {
    this.disabledBtn = true;
    this.mainService.update(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Cliente Actualizado', 'El Cliente fue actualizado exitosamente.');
      this.closeModal('Close');
    },(error) => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //CERRAR MODAL
  closeModal(alert?:any) {
    this.modalController.dismiss(alert);
  }

  getPosition():any{
    this.geolocation.getCurrentPosition()
    .then(resp => {
      this.data.latitud = resp.coords.latitude;
      this.data.longitud = resp.coords.longitude;
      console.log(resp.coords.latitude + " dasdsa "+ resp.coords.longitude)
      this.loadMap(resp.coords.latitude, resp.coords.longitude);
      this.notificationService.dismiss();
     }).catch((error) => {
      this.notificationService.dismiss();
    });
  }

  //CARGAR MAPA ACTUALIZAR
  public loadMapUpdate(lat:any, lon:any) {
    let latitude = lat;
    let longitude = lon;
    this.data.latitud = latitude.toString();
    this.data.longitud = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('mapmodalprofile');
    
    var myLatLng = new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude));
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    var marker;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.data.latitud = evt.latLng.lat();
      this.data.longitud = evt.latLng.lng();
    });
  }

  public loadMap(lat:any, lon:any) {
    let latitude = lat;
    let longitude = lon;
    this.data.latitud = latitude.toString();
    this.data.longitud = longitude.toString();
    console.log(this.data.latitud)
    console.log(this.data.longitud)


    let mapEle: HTMLElement = document.getElementById('mapmodalprofile');
    /*console.log(mapEle)
    let myLatLng = new google.maps.LatLng({lat: parseFloat(latitude), lng: parseFloat(longitude)});
    console.log(myLatLng)
    this.map = await new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });*/

    var myLatLng = new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude));
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });
  
    var marker;
    marker = new google.maps.Marker({
        map: this.map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: myLatLng
    });
  
    google.maps.event.addListener(marker, 'dragend', (evt) => {
        this.data.latitud = evt.latLng.lat();
        this.data.longitud = evt.latLng.lng();
    });
  }

}
