import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController, AlertController, IonInfiniteScroll } from '@ionic/angular';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { ClienteService } from 'src/app/_services/cliente.service';
import { ClienteUsuarioService } from 'src/app/_services/cliente-usuario.service';
import { ModalClienteComponent } from './modal-cliente/modal-cliente.component';
import { Network } from '@ionic-native/network/ngx';
import { NetworkService } from 'src/app/_services/network.service';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.page.html',
  styleUrls: ['./cliente.page.scss'],
})
export class ClientePage implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  private title:string = 'Clientes';
  private table:any[];
  private table2:any[];
  private itemSelected:string = 'mis';
  idUser:any = localStorage.getItem('currentId')
  private disabledBtn:boolean;
  private longitud:number = 0;

  constructor(private router: Router,
    private location: Location,
    private modalController: ModalController,
    private notificationService: NotificacionService,
    private alertController: AlertController,
    private mainService: ClienteService,
    private secondService: ClienteUsuarioService,
    private networkService: NetworkService
  ) {
  }

  ngOnInit() {
  }

  //SIGUIENTE
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  //ANTERIOR
  goToBack() {
    this.location.back();
  }

  //SEGMENT CHANGED
  segmentChanged(ev: any) {
    this.itemSelected = ev.detail.value;
    /*if(this.itemSelected == 'mis') {
      this.table = JSON.parse(localStorage.getItem('currentMyClients'));
    } else if(this.itemSelected == 'todos') {
      this.longitud = 0;
      this.table2 = [];
      for (let i = 0; i < 50; i++) {
        this.table2.push(JSON.parse(localStorage.getItem('currentAllClients'))[i]);
        this.longitud++;
      }
      console.log("PRIMERA VEZ" +this.longitud)
    }*/
  }

  async getMyClients() {
    if(this.networkService.isOnline()) {
      const alert = await this.alertController.create({
        header: 'Sincronizar Datos',
        message: '¿Desea sincronizar la información de sus clientes?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'OK',
            handler: () => {
              console.log('Confirm Okay');
              this.getAllMyUsers();
            }
          }
        ]
      });
      await alert.present();
    } else {
      this.notificationService.alertToast('La red ha sido desconectada D:');
    }
  }

  async getAllClients() {
    if(this.networkService.isOnline()) {
      const alert = await this.alertController.create({
        header: 'Sincronizar Datos',
        message: '¿Desea sincronizar la información de todos los clientes?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'OK',
            handler: () => {
              console.log('Confirm Okay');
              this.getAll();
            }
          }
        ]
      });
      await alert.present();
    } else {
      this.notificationService.alertToast('La red ha sido desconectada D:');
    }
  }

  //OPEN MODAL
  async presentModal(id?:number) {
    const modal = await this.modalController.create({
      component: ModalClienteComponent,
      componentProps: { value: id }
    });
    modal.onDidDismiss().then((data) => {
      if(data.data) {
        if(this.networkService.isOnline()) {
          this.getAllMyUsers();
        } else {
          this.notificationService.alertToast('La red ha sido desconectada D:');
        }        
      }
    });
    return await modal.present();
  }

  //GET ALL MY USERS
  public getAllMyUsers() {
    this.notificationService.alertLoading('Cargando...', 10000);
    this.mainService.getAllByUser(this.idUser)
    .subscribe((res) => {
      localStorage.setItem('currentMyClients', JSON.stringify(res));
      this.getAllMine();
      this.notificationService.dismiss();
    },(error) => {
      this.notificationService.dismiss();
      console.error(error)
    })
  }

  //GET ALL
  async getAll() {
    this.notificationService.alertLoading('Cargando...', 10000);
    this.mainService.getAll()
    .subscribe((res) => {
      //this.table2 = [];
      //this.table2 = res;
      localStorage.setItem('currentAllClients', JSON.stringify(res));
      this.getAllClientes();
      this.notificationService.dismiss();
    },(error) => {
      this.notificationService.dismiss();
      console.error(error)
    })
  }

  //ELIMINAR
  delete(id:any) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Cliente Eliminado', 'El cliente fue eliminado exitosamente.');
      this.getAllMyUsers();
      this.disabledBtn = false;
    },(error) => {
      this.disabledBtn = false;
      console.error(error)
    });
  }

  //DELETE
  async confirmation(data:any) {
    if(this.networkService.isOnline()) {
    this.disabledBtn = true;
    const alert = await this.alertController.create({
      header: 'Eliminar Cliente',
      message: '¿Desea eliminar al cliente '+ data.nombre + ' ' + data.apellido + ' ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            this.disabledBtn = false;
          }
        }, {
          text: 'OK',
          handler: () => {
            console.log('Confirm Okay');
            this.delete(data.id);
          }
        }
      ]
    });
    await alert.present();
    } else {
      this.notificationService.alertToast('La red ha sido desconectada. Por favor, inténtelo más tarde.');
    }
  }

  //AGREGAR
  assignament(id:number) {
    if(this.networkService.isOnline()) {
      let data = {
        clientes: [{id:id}],
        usuario: localStorage.getItem('currentId')
      }
      this.disabledBtn = true;
      this.secondService.create(data)
      .subscribe((res) => {
        this.notificationService.alertMessage('Cliente Asignado', 'El cliente fue asignado exitosamente.');
        this.getAllMyUsers();
        this.disabledBtn = false;
      },(error) => {
        this.notificationService.alertMessage('Cliente No Asignado', 'Error de asignación.');
        this.disabledBtn = false;
        console.log(error)
      });
    } else {
      this.notificationService.alertToast('La red ha sido desconectada. Por favor, inténtelo más tarde');
    }
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      if(this.itemSelected == 'mis') {
        this.table = JSON.parse(localStorage.getItem('currentMyClients'));
        
      } else if(this.itemSelected == 'todos') {
        //this.table = JSON.parse(localStorage.getItem('currentAllClients'));
        for (let i = 0; i < 50; i++) {
          this.table2.push(JSON.parse(localStorage.getItem('currentAllClients'))[this.longitud + i]);
          this.longitud++;
        }
        console.log("PRIMERA VEZ" +this.longitud)
      }
      console.log('Async operation has ended');
      infiniteScroll.target.complete();
    }, 500);
  }

  getAllMine() {
    this.table = [];
    this.table = JSON.parse(localStorage.getItem('currentMyClients'));
  }

  getAllClientes() {
    this.longitud = 0;
    this.table2 = [];
    for (let i = 0; i < 50; i++) {
      this.table2.push(JSON.parse(localStorage.getItem('currentAllClients'))[i]);
      this.longitud++;
    }
  }
}
