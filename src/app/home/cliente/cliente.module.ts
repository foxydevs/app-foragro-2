import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ClientePage } from './cliente.page';
import { ModalClienteComponent } from './modal-cliente/modal-cliente.component';
import { ClienteService } from 'src/app/_services/cliente.service';
import { ClienteUsuarioService } from 'src/app/_services/cliente-usuario.service';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

const routes: Routes = [
  {
    path: '',
    component: ClientePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ClientePage,
    ModalClienteComponent
  ],
  entryComponents: [
    ModalClienteComponent
  ],
  providers: [
    ClienteService,
    ClienteUsuarioService
  ]
})
export class ClientePageModule {}
