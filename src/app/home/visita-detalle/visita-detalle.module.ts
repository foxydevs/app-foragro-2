import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VisitaDetallePage } from './visita-detalle.page';
import { ModalVisitaDetalleComponent } from './modal-visita-detalle/modal-visita-detalle.component';

const routes: Routes = [
  {
    path: '',
    component: VisitaDetallePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    VisitaDetallePage,
    ModalVisitaDetalleComponent
  ],
  entryComponents: [
    ModalVisitaDetalleComponent
  ]
})
export class VisitaDetallePageModule {}
