import { Component, OnInit } from '@angular/core';
import { path } from 'src/app/config.module';
import { ModalController, NavParams } from '@ionic/angular';
import { FormVisitaService } from 'src/app/_services/form-visita.service';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { NetworkService } from 'src/app/_services/network.service';
import { ClienteUsuarioService } from 'src/app/_services/cliente-usuario.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

//JQUERY
declare var $:any;

@Component({
  selector: 'app-modal-visita-detalle',
  templateUrl: './modal-visita-detalle.component.html',
  styleUrls: ['./modal-visita-detalle.component.scss'],
})
export class ModalVisitaDetalleComponent implements OnInit {
  private title:string = 'Formulario Cliente';
  private idUserApp:any = localStorage.getItem('currentId');
  private parameter:any;
  private data2:any;
  basePath:string = path.path;
  data3 ={
    semanas: '',
    fechas: '',
    horas: '',
    nombre_clientes: '',
    lugars: '',
    departamentos: '',
    objetivo: '', 
    cultivos: '',
    foto1: 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
    foto2: 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
    foto3: 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
    observacion: '',
    tipo: '',
    tipo_clientes: '',
    tipo_avances: '',
    longitud_inicial: 0,
    longitud_final: 0,
    latitud_inicial: 0,
    latitud_final: 0,
    hora_inicial: '',
    hora_final: '',
  }
  data = {
    id: '',
    semana: '',
    fecha: '',
    hora: '',
    nombre_cliente: '',
    lugar: '',
    departamento: '',
    objetivos: '', 
    cultivo: '',
    observaciones: '',
    tipo: '',
    estado: 0,
    cliente: 1,
    tipo_cliente: '',
    tipo_avance: '',
    foto1: 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
    foto2: 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
    foto3: 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
    logrado: 1,
    razon: '',
    planificacion: '',
    usuario: localStorage.getItem('currentId'),
    longitud_inicial: 0,
    longitud_final: 0,
    latitud_inicial: 0,
    latitud_final: 0,
    hora_inicial: '',
    hora_final: '',
  }
  dataRow = {
    id_encabezado: localStorage.getItem('currentRouteID'),
    id_visita: 1,
    longitud: 0,
    latitud: 0
  }
  private disabledBtn:boolean = false;
  private selectedItem:any = 'plan'
  private tipoCliente:any = [];
  private tipoAvance:any = [];
  private cultivos:any = [];
  private objetivo:any = [];

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private notificationService: NotificacionService,
    private networkService: NetworkService,
    private mainService: FormVisitaService,
    private secondService: ClienteUsuarioService,
    private geolocation: Geolocation,
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value');
    this.dataRow.id_visita = this.parameter;
    if(this.networkService.isOnline()) {
      this.notificationService.alertLoading('Cargando...', 10000);
      this.getSingle(this.parameter);
    } else {
      this.notificationService.alertToast('La red ha sido desconectada. No se pueden realizar cambios por favor, intente más tarde.');
    }
    this.tipoCliente = JSON.parse(localStorage.getItem('currentTipoCliente'));
    this.tipoAvance = JSON.parse(localStorage.getItem('currentTipoAvance'));
    this.objetivo = JSON.parse(localStorage.getItem('currentTipoObjetivos'));
    this.cultivos = JSON.parse(localStorage.getItem('currentTipoCultivo'));
  }

  //SEGMENT CHANGED
  segmentChanged(ev: any) {
    this.selectedItem = ev.detail.value;
  }

  //GET DATE
  getDate() {
    let fechahoy = new Date();
    this.data.fecha = fechahoy.getFullYear() + '-' + (fechahoy.getMonth() + 1) + '-' + fechahoy.getDate();
    this.data.hora = fechahoy.getHours() + ':' + fechahoy.getMinutes() + ':' + fechahoy.getSeconds();
  }

  //SAVE CHANGES
  async saveChanges(e:any) {
    this.data.foto1 = $('img[alt="Avatar"]').attr('src');
    this.data.foto2 = $('img[alt="Avatar2"]').attr('src');
    this.data.foto3 = $('img[alt="Avatar3"]').attr('src');

    if(this.data.foto1 == 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png') {
      this.data.foto1 = '';
    }
    if(this.data.foto2 == 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png') {
      this.data.foto2 = '';
    }
    if(this.data.foto3 == 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png') {
      this.data.foto3 = '';
    }
    this.data.tipo_cliente = e.tipo_cliente;
    this.data.objetivos = e.objetivos;
    this.data.cultivo = e.cultivo;
    this.data.tipo_avance = e.tipo_avance;
    await this.geolocation.getCurrentPosition()
    .then(resp => {
      var d = new Date();
      this.data.latitud_final =  resp.coords.latitude;
      this.data.longitud_final =  resp.coords.longitude;
      this.data.hora_final= d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear() + "  "  + d.getHours() +":" +d.getMinutes();
      this.create(this.data);
     }).catch((error) => {
    });
    let data = {
      logrado: this.data.logrado,
      id: this.data.id,
      estado: 0,
      foto1: this.data.foto1,
      foto2: this.data.foto2,
      foto3: this.data.foto3,
    }
    await this.changeState(+localStorage.getItem('currentUsuarioCliente'), 2);
    await this.updated(data)
    this.dataRow.id_encabezado = localStorage.getItem('currentRouteID')
    this.dataRow.latitud = this.data.latitud_final;
    this.dataRow.longitud = this.data.longitud_final;
    console.log(this.dataRow);
    await this.createDetail(this.dataRow);
  }

  actualizarFormularioInicial() {
    var d = new Date();
    this.geolocation.getCurrentPosition()
    .then(resp => {
      let data = {
        id: this.data.id,
        latitud_inicial: resp.coords.latitude,
        longitud_inicial: resp.coords.longitude,
        hora_inicial: d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear() + "  "  + d.getHours() +":" +d.getMinutes()
      }
      console.log(data)
      this.mainService.update(data)
      .subscribe((res) => {
        this.data.hora_inicial = data.hora_inicial;
        this.data.latitud_inicial = data.latitud_inicial;
        this.data.longitud_inicial = data.longitud_inicial;
        this.notificationService.alertMessage("Planificacion", "Inicio de Planificación Registrado.");
      },(error) => {
        console.error(error)
      });
     }).catch((error) => {
    });
  }

  //GET SINGLE
  public getSingle(id:any){
    this.mainService.getSingle(id)
    .subscribe((res) => {
      console.log(res)
      this.data2 = res;
      this.data = res;
      this.data.foto1 = 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
      this.data.foto2 = 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
      this.data.foto3 = 'https://mbtskoudsalg.com/images/is-png-a-vector-file.png',
      this.data3.semanas = res.semana;
      this.data3.fechas = res.fecha;
      this.data3.horas = res.hora;
      this.data3.nombre_clientes = res.nombre_cliente;
      this.data3.lugars = res.lugar;
      this.data3.departamentos = res.departamento;
      this.data3.objetivo = res.objetivo.nombre;
      this.data3.observacion = res.observaciones;
      this.data3.tipo_clientes = res.tipos_cliente.nombre;
      this.data3.tipo_avances = res.tipos_avance.nombre;
      this.data3.cultivos = res.cultivos.nombre;
      this.data.planificacion = this.parameter;
      this.getDate();
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();
      console.error(error);
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.disabledBtn = true;
    this.mainService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Planificación Agregada', 'La planificación fue agregada exitosamente.');
      this.closeModal('Close');
    },(error) => {
      this.disabledBtn = false;
      console.error(error)
    });
  }

  //ACTUALIZAR
  updated(formValue:any) {
    this.mainService.update(formValue)
    .subscribe((res) => {
    },(error) => {
      console.error(error)
    });
  }

  //CERRAR MODAL
  closeModal(alert?:any) {
    this.modalController.dismiss(alert);
  }
  //IMAGEN DE CATEGORIA
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `http://backend.foxylabs.xyz/presentacion/public/api/pictures/upload`;
  
    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'visitas'
          },
          function(respuesta) {
            console.log(respuesta.url)
            $('#imgAvatar').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.notificationService.alertToast('La imagen es demasiado grande.')
      }
    } else {
      this.notificationService.alertToast('El tipo de imagen no es válido.')
    }
  }

  //IMAGEN DE CATEGORIA
  uploadImage2(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `http://backend.foxylabs.xyz/presentacion/public/api/pictures/upload`;
    console.log(archivos)
    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        $('#imgAvatar2').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'visitas'
          },
          function(respuesta) {
            console.log(respuesta.url);

            $('#imgAvatar2').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
    } else {
      this.notificationService.alertToast('El tipo de imagen no es válido.')
    }
  }

  //IMAGEN DE CATEGORIA
  uploadImage3(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `http://backend.foxylabs.xyz/presentacion/public/api/pictures/upload`;
    console.log(archivos)

    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        $('#imgAvatar3').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'visitas'
          },
          function(respuesta) {
            console.log(respuesta.url);

            $('#imgAvatar3').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
    } else {
      this.notificationService.alertToast('El tipo de imagen no es válido.')
    }
  }

  //CAMBIAR ESTADO
  changeState(id:any, state:any) {
    let data = {
      id: id,
      estado: state,
    }
    this.secondService.update(data)
    .subscribe((res) => {
    }, (error) => {
      console.clear
    })
  }

  createDetail(formValue:any) {
    this.mainService.createRowRoute(formValue)
    .subscribe((res) => {
      console.log(res);
    },(error) => {
      console.error(error)
    });
  }
}
