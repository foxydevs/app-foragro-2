import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { FormVisitaService } from 'src/app/_services/form-visita.service';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { NetworkService } from 'src/app/_services/network.service';
import { ModalVisitaDetalleComponent } from './modal-visita-detalle/modal-visita-detalle.component';

@Component({
  selector: 'app-visita-detalle',
  templateUrl: './visita-detalle.page.html',
  styleUrls: ['./visita-detalle.page.scss'],
})
export class VisitaDetallePage implements OnInit {
  private title:string = 'Detalle Planificación'
  private visitas:any[];
  private parameter:any;
  private selectedItem:any = 'pendiente';

  constructor(
    private router: Router,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private modalController: ModalController,
    private mainService: FormVisitaService,
    private notificationService: NotificacionService,
    private networkService: NetworkService,
  ) { }

  ngOnInit() {
    this.parameter = this.activatedRoute.snapshot.paramMap.get('id');
  }

  //SIGUIENTE
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  //ANTERIOR
  goToBack() {
    this.location.back();
  }

  //SEGMENT CHANGED
  segmentChanged(ev: any) {
    this.selectedItem = ev.detail.value;
    if(this.networkService.isOnline()) {
      this.getAll(this.parameter);
    } else {
      this.notificationService.alertToast('La red ha sido desconectada D:');
    }
  }

  //CARGAR
  public getAll(id:any){
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getAllClient(id)
    .subscribe((res) => {
      this.visitas = res;
      this.visitas.reverse();
      this.notificationService.dismiss();
    },(error) => {
      this.notificationService.dismiss();
      console.error(error)
    })
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalVisitaDetalleComponent,
      componentProps: { value: id }
    });
    modal.onDidDismiss().then((data) => {
      if(this.networkService.isOnline()) {
        this.getAll(this.parameter);
      } else {
        this.notificationService.alertToast('La red ha sido desconectada D:');
      }
    });
    return await modal.present();
  }

}
