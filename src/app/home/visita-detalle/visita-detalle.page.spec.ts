import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitaDetallePage } from './visita-detalle.page';

describe('VisitaDetallePage', () => {
  let component: VisitaDetallePage;
  let fixture: ComponentFixture<VisitaDetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitaDetallePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitaDetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
