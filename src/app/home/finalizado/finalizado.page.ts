import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { ClienteService } from 'src/app/_services/cliente.service';
import { NetworkService } from 'src/app/_services/network.service';

@Component({
  selector: 'app-finalizado',
  templateUrl: './finalizado.page.html',
  styleUrls: ['./finalizado.page.scss'],
})
export class FinalizadoPage implements OnInit {
  private title = 'Rutas Terminadas';
  private clientes:any[];

  constructor(
    private router: Router,
    private location: Location,
    private notificationService: NotificacionService,
    private networkService: NetworkService,
    private mainService: ClienteService
  ) { }

  ngOnInit() {
    if(this.networkService.isOnline()) {
      this.getAll();
    } else {
      this.notificationService.alertToast('La red ha sido desconectada. Por favor, inténtelo más tarde.');
    }
  }

  //SIGUIENTE
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  //ANTERIOR
  goToBack() {
    this.location.back();
  }

  //CARGAR
  public getAll() {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getAllByUser(+localStorage.getItem('currentId'))
    .subscribe((res) => {
      this.clientes = [];
      res.forEach(x => {
        let data = {
          nombre: x.nombre + ' ' + x.apellido,
          direccion: x.direccion,
          id: x.id,
          estado: x.estado 
        }
        this.clientes.push(data)
      });
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();
      console.clear
    })
  }

}
