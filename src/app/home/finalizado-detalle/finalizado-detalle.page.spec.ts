import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalizadoDetallePage } from './finalizado-detalle.page';

describe('FinalizadoDetallePage', () => {
  let component: FinalizadoDetallePage;
  let fixture: ComponentFixture<FinalizadoDetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinalizadoDetallePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalizadoDetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
