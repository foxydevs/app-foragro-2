import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FinalizadoDetallePage } from './finalizado-detalle.page';

const routes: Routes = [
  {
    path: '',
    component: FinalizadoDetallePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FinalizadoDetallePage]
})
export class FinalizadoDetallePageModule {}
