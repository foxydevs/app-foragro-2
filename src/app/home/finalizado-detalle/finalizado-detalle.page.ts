import { Component, OnInit } from '@angular/core';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { FormVisitaService } from 'src/app/_services/form-visita.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { NetworkService } from 'src/app/_services/network.service';
import { ModalVisitaComponent } from '../visita/modal-visita/modal-visita.component';

@Component({
  selector: 'app-finalizado-detalle',
  templateUrl: './finalizado-detalle.page.html',
  styleUrls: ['./finalizado-detalle.page.scss'],
})
export class FinalizadoDetallePage implements OnInit {
  private title = 'Detalle de Ruta';
  private parameter:any;
  private visitas:any[];

  constructor(
  public notificationService: NotificacionService,
  public networkService: NetworkService,
  public mainService: FormVisitaService,
  public router: ActivatedRoute,
  public location: Location,
  public modalController: ModalController,
  ) { }

  ngOnInit() {
    this.parameter = this.router.snapshot.paramMap.get('id');
    if(this.networkService.isOnline()) {
      this.getAll(this.parameter);
    } else {
      this.notificationService.alertToast('La red ha sido desconectada. Por favor, inténtelo más tarde.');
    }    
  }

  //ROUTES
  goToBack() {
    this.location.back();
  }

  //CARGAR
  public getAll(id:number) {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.mainService.getAllClient(id)
    .subscribe(response => {
      this.visitas = [];
      this.visitas = response;
      this.visitas.reverse();
      this.notificationService.dismiss();
    },(error) => {
      console.error(error);
      this.notificationService.dismiss();
    })
  }

  async presentModalVisita(id:number) {
    const modal = await this.modalController.create({
      component: ModalVisitaComponent,
      componentProps: { value: id }
    });
    return await modal.present();
  }

}
