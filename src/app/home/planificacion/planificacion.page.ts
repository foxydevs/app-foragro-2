import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { LoadingController, ModalController, Platform, ActionSheetController } from '@ionic/angular';
import { ClienteService } from 'src/app/_services/cliente.service';
import { ClienteUsuarioService } from 'src/app/_services/cliente-usuario.service';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { NetworkService } from 'src/app/_services/network.service';
import { FormVisitaService } from 'src/app/_services/form-visita.service';

//GOOGLE
declare var google;

@Component({
  selector: 'app-planificacion',
  templateUrl: './planificacion.page.html',
  styleUrls: ['./planificacion.page.scss'],
})
export class PlanificacionPage implements OnInit {
  private title:string = 'Planificaciones';
  selectedTag: any = 'ventas';
  clientes:any[] = [];
  clientesPendientes:any[] = [];
  clientesPendi:any[] = [];
  idUserApp:any = localStorage.getItem('currentId');
  //PROPIEDADES GOOGLE MAPS
  map: any;
  directionsService: any = null;
  directionsDisplay: any = null;
  bounds: any = null;
  myLatLng: any;
  waypoints: any[];
  latitude = 0;
  longitude = 0;
  estado:boolean = true;
  buttonSendMail:boolean;

  constructor(
    private router: Router,
    private location: Location,
    private mainService: ClienteService,
    private secondService: ClienteUsuarioService,
    private notificationService: NotificacionService,
    private geolocation: Geolocation,
    private actionSheetController: ActionSheetController,
    private launchNavigator: LaunchNavigator,
    private networkService: NetworkService,
    private thirdService: FormVisitaService
  ) {
    
  }

  ngOnInit() {
    if(this.networkService.isOnline()) {
      this.notificationService.alertLoading('Cargando...', 15000);
      this.getPosition();
    } else {
      this.notificationService.alertToast('La red ha sido desconectada. El mapa no puede visualizarse.');
    }
    //BOTON SEMANA
    if(localStorage.getItem('currentStart') == 'true') {
      this.buttonSendMail = true;
    } else {
      this.buttonSendMail = false;
    }
  }

  //SIGUIENTE
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  //ATRAS
  goToBack() {
    this.location.back();
  }

  //GET ALL CLIENTS
  getAll(id:any, lat:any, lng:any) {
    if(this.networkService.isOnline()) {
      this.mainService.getNear(id, lat, lng)
      .subscribe((res) => {
        this.clientes.length = 0;
        this.clientesPendientes.length = 0;
        this.clientesPendi.length = 0;
        res.forEach(x => {
          let data = {
            nombre: x.nombre?x.nombre:'',
            apellido: x.apellido?x.apellido:'',
            direccion: x.direccion?x.direccion:'',
            telefono: x.telefono?x.telefono:'',
            estado: x.estado?x.estado:'',
            latitud: x.latitud?x.latitud:'', 
            longitud: x.longitud?x.longitud:'',
            distance: x.distance?parseFloat(x.distance).toFixed(2) + ' K':'',
            id: x.id,
            usuarioidDelete: x.usuarioidDelete
          }
          if(data.estado==3) {
            this.clientesPendi.push(data)
          }
          if(data.estado==2) {
            this.clientesPendientes.push(data)
          }
          if(data.estado==1) {
            this.clientes.push(data)
          }
        })
        this.getMap();
        if(this.clientes.length > 0) {
          this.estado = false;
        } else {
          this.estado = true;
        }
        localStorage.setItem('currentClientes', JSON.stringify(this.clientes));
      },(error) => {
        console.error(error)
      })
    } else {
      this.clientes = JSON.parse(localStorage.getItem('currentClientes'));     
    }    
  }

  async getLocation(latitude:any, longitude:any, data?:any) {
    const actionSheet = await this.actionSheetController.create({
      header: 'App de Ubicación',
      buttons: [
      {
        text: 'Google Maps',
        icon: 'map',
        handler: () => {
          let options: LaunchNavigatorOptions = {
            start: `${this.latitude},${this.longitude}`,
            app: this.launchNavigator.APP.GOOGLE_MAPS
          }
          
          this.launchNavigator.navigate([latitude, longitude], options)
          .then(
            success => console.log('Launched navigator'),
            error => console.log('Error launching navigator', error)
          );
        }
      }, {
        text: 'Waze',
        icon: 'pin',
        handler: () => {
          let options: LaunchNavigatorOptions = {
            start: `${this.latitude},${this.longitude}`,
            app: this.launchNavigator.APP.WAZE
          }
          
          this.launchNavigator.navigate([latitude, longitude], options)
          .then(
            success => console.log('Launched navigator'),
            error => console.log('Error launching navigator', error)
          );
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  //CARGAR RUTAS
  private getAllRoute(id:any, lat:any, lng:any) {
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    this.bounds = new google.maps.LatLngBounds();
    this.mainService.getNear(id, lat, lng)
    .subscribe((res) => {
      this.waypoints = [];
      res.forEach(x => {
        if(x.latitud && x.estado == '1') {
          let data2 = {
            location: {lat: parseFloat(x.latitud), lng: parseFloat(x.longitud)},
            stopover: true,
          }
          this.waypoints.push(data2)
        }
      });
      this.notificationService.dismiss();
    },(error) => {
      console.error(error)
    })
  }

  //CAMBIAR ESTADO
  changeState(id:any, state:any) {
    this.notificationService.alertLoading("Cargando...", 5000);
    let data = {
      id: id,
      estado: state,
    }
    this.secondService.update(data)
    .subscribe((res) => {
      this.getPosition();
      setTimeout(() => {
        this.getMap();
      }, 4000);
    }, (error) => {
      console.clear
    })
  }
  
  //GET POSITION
  getPosition() : void {
    this.geolocation.getCurrentPosition()
    .then(resp => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      this.getAllRoute(this.idUserApp, this.latitude, this.longitude);
      this.getAll(this.idUserApp, this.latitude, this.longitude);
     }).catch(error => {
    });
  }
  
  getMap() {
    // create a new map by passing HTMLElement
    let mapEle: HTMLElement = document.getElementById('map-planificacion');
  
    // create LatLng object
    this.myLatLng = {lat: this.latitude, lng: this.longitude};
  
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: this.myLatLng,
      zoom: 17
    });
  
    this.directionsDisplay.setMap(this.map);
  
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      mapEle.classList.add('show-map');
      this.calculateRoute();
    });
  }
  
  private calculateRoute(){
    if(this.waypoints.length > 0) {    
    this.bounds.extend(this.myLatLng);
  
    this.waypoints.forEach(waypoint => {
      var point = new google.maps.LatLng(waypoint.location.lat, waypoint.location.lng);
      this.bounds.extend(point);
    });
  
    this.map.fitBounds(this.bounds);
  
    this.directionsService.route({
      origin: new google.maps.LatLng(this.latitude, this.longitude),
      destination: new google.maps.LatLng(this.waypoints[this.waypoints.length -1].location.lat, this.waypoints[this.waypoints.length -1].location.lng),
      waypoints: this.waypoints,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING,
      avoidTolls: true
    }, (res, status)=> {
      if(status === google.maps.DirectionsStatus.OK) {
        this.directionsDisplay.setDirections(res);
      }else{
        alert('Could not display directions due to: ' + status);
      }
    });  
    } else {
      //this.notificationService.alertToast('No existen datos cargados.');      
    }
  }
  
  doRefresh(event) {
    setTimeout(() => {
      if(this.networkService.isOnline()) {
        this.getMap();
      } else {
        this.notificationService.alertToast('La red ha sido desconectada. El mapa no puede visualizarse.');
      }
      event.target.complete();
    }, 2000);
  }

  sendMail() {
    /*var d = new Date();
    let fechaInicio = d.getFullYear() + '-' + (d.getMonth() +1) + '-' + d.getDate();
    console.log(fechaInicio)
    this.thirdService.sendMail(this.idUserApp, fechaInicio, fechaInicio, 0)
    .subscribe((res) => {
      this.notificationService.alertMessage('Planificación Enviada', 'La planificación ha sido enviada.')
    }, (error) => {
      console.error(error);
    });*/
    localStorage.setItem('currentStart', 'true');
    this.buttonSendMail = true;
    console.log(this.buttonSendMail)
  }

}
