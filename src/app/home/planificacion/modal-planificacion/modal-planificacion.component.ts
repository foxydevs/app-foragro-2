import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { FormVisitaService } from 'src/app/_services/form-visita.service';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { NetworkService } from 'src/app/_services/network.service';
import * as moment from 'moment';
import { ClienteService } from 'src/app/_services/cliente.service';

@Component({
  selector: 'app-modal-planificacion',
  templateUrl: './modal-planificacion.component.html',
  styleUrls: ['./modal-planificacion.component.scss'],
})
export class ModalPlanificacionComponent implements OnInit {
  //PROPIEDADES
  private disabledBtn:boolean;
  private parameter:any;
  private title:string = 'Formulario Planificación';
  private tipoCliente:any = [];
  private tipoAvance:any = [];
  private cultivos:any = [];
  private objetivos:any = [];
  data = {
    id: '',
    semana: '',
    fecha: new Date().toISOString(),
    hora: new Date().toISOString(),
    nombre_cliente: '',
    lugar: '',
    departamento: '',
    objetivos: '', 
    cultivo: '',
    observaciones: '',
    tipo: '',
    estado: 1,
    cliente: '',
    tipo_cliente: '',
    tipo_avance: '',
    latitud: '',
    longitud: '',
    logrado: null,
    razon: '',
    usuario: localStorage.getItem('currentId'),
  }

  constructor(
    private navParams: NavParams,
    private mainService: FormVisitaService,
    private secondService: ClienteService,
    private notificationService: NotificacionService,
    private networkService: NetworkService,
    private modalController: ModalController
  ) {
    
  }

  ngOnInit() {
    this.data.cliente = this.navParams.get('cliente');
    this.getSingleCliente(this.data.cliente);
    this.tipoCliente = JSON.parse(localStorage.getItem('currentTipoCliente'));
    this.tipoAvance = JSON.parse(localStorage.getItem('currentTipoAvance'));
    this.objetivos = JSON.parse(localStorage.getItem('currentTipoObjetivos'));
    this.cultivos = JSON.parse(localStorage.getItem('currentTipoCultivo'));
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    this.data.fecha = moment(this.data.fecha).format('YYYY-MM-DD');
    if(this.networkService.isOnline()) {
      this.create(this.data)
    } else {
      this.notificationService.alertToast('La red ha sido desconectada.');
      this.disabledBtn = true;
      this.createOffline(this.data);
      this.notificationService.alertMessage('Sin Conexión', 'El formulario fue agregado en la sección SIN CONEXIÓN.');
      this.closeModal(this.data);
    }    
  }

  //GET SINGLE
  public getSingleCliente(id:any){
    this.secondService.getSingle(id)
    .subscribe((res) => {
      console.log(res)
      this.data.nombre_cliente = res.nombre + ' ' + res.apellido;
      this.data.latitud = res.latitud;
      this.data.longitud = res.longitud;
      this.data.lugar = res.direccion;
      this.data.departamento = res.departamento;
    },(error) => {
      console.clear;
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.disabledBtn = true;
    this.mainService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Planificación Agregada', 'La planificación fue agregada exitosamente.');
      this.closeModal(res);
    },(error) => {
      this.disabledBtn = false;
      console.error(error)
    });
  }

  //AGREGAR
  createOffline(data:any) {
    let client:any[] = []
    if(localStorage.getItem('currentVisitaOffline')) {
      client = JSON.parse(localStorage.getItem('currentVisitaOffline'))
    }
    client.push(data)
    localStorage.removeItem('currentVisitaOffline')
    localStorage.setItem('currentVisitaOffline', JSON.stringify(client));
  }

  //CERRAR MODAL
  closeModal(alert?:any) {
    this.modalController.dismiss(alert);
  }

}
