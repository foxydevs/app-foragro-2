import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { ModalPerfilComponent } from './modal-perfil/modal-perfil.component';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  private title:string = 'Mi Perfil'
  data = {
    picture: localStorage.getItem('currentPicture'),
    nombre: localStorage.getItem('currentFirstName') + ' ' + localStorage.getItem('currentLastName'),
    email: localStorage.getItem('currentEmail'),
    id: localStorage.getItem('currentId'),
  }
  
  constructor(
    public router: Router,
    public location: Location,
    private modalController: ModalController
  ) { }

  ngOnInit() {
  }

  //SIGUIENTE
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  //ANTERIOR
  goToBack() {
    this.location.back();
  }

  getAll() {
    this.data.picture = localStorage.getItem('currentPicture')
    this.data.nombre = localStorage.getItem('currentFirstName') + ' ' + localStorage.getItem('currentLastName')
    this.data.email = localStorage.getItem('currentEmail')
  }

  //PRESENT MODAL
  async presentModal(id?:number) {
    const modal = await this.modalController.create({
      component: ModalPerfilComponent,
      componentProps: { value: id }
    });
    modal.onDidDismiss().then((data) => {
      this.getAll();
    });
    return await modal.present();
  }

}
