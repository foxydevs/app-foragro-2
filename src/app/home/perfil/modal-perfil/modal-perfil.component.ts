import { Component, OnInit } from '@angular/core';
import { path } from 'src/app/config.module';
import { ModalController, NavParams } from '@ionic/angular';
import { UsuarioService } from 'src/app/_services/usuario.service';
import { EmpleadoService } from 'src/app/_services/empleado.service';
import { NotificacionService } from 'src/app/_services/notificacion.service';

//JQUERY
declare var $:any;

@Component({
  selector: 'app-modal-perfil',
  templateUrl: './modal-perfil.component.html',
  styleUrls: ['./modal-perfil.component.scss'],
})
export class ModalPerfilComponent implements OnInit {
  private parameter:any;
  private title:any;
  private table:any = [];
  private btnDisabled:boolean;
  private btnDisabled2:boolean;
  private basePath:string = path.path;
  changePassword = {
    old_pass: '',
	  new_pass : '',
	  new_pass_rep: '',
    id: +localStorage.getItem('currentId')
  }
  data = {
    nombre: '',
    apellido: '',
    direccion: '',
    telefono: '',
    celular: '',
    id: +localStorage.getItem('currentEmpleadoID')
  }
  data2 = {
    pais: '',
    picture: '',
    id: +localStorage.getItem('currentId')
  }

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private mainService: UsuarioService,
    private secondService: EmpleadoService,
    private notificationService: NotificacionService,
  ) { 
  }

  ngOnInit() {
    this.table = JSON.parse(localStorage.getItem('currentPaises'))
    this.parameter = this.navParams.get('value');
    if(this.parameter == '1') {
      this.title = 'Actualizar Perfil';
      this.getSingle(this.data2.id)
      this.getSingleEmpleado(this.data.id)
    } else if(this.parameter == '2') {
      this.title = 'Cambiar Contraseña';
    }
  }

  saveChanges(e:any) {
    this.data2.pais = e.pais;
    this.updated(this.data2);
    this.updatedEmpleado(this.data);
  }

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  //ACTUALIZAR
  updated(formValue:any) {
    this.btnDisabled = true;
    this.mainService.update(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Perfil Actualizado', 'Tu perfil ha sido actualizado exitosamente.');
      this.closeModal();
    }, (error) => {
      this.btnDisabled = false;
      console.error(error)
    });
  }

  //ACTUALIZAR
  updatedEmpleado(formValue:any) {
    this.btnDisabled = true;
    this.secondService.update(formValue)
    .subscribe(res => {
      localStorage.setItem('currentFirstName', res.nombre);
      localStorage.setItem('currentLastName', res.apellido);
    }, (error) => {
      this.btnDisabled = false;
      console.error(error)
    });
  }

  //CARGAR USUARIO
  public getSingle(id:any) {
    this.mainService.getSingle(id)
    .subscribe((res) => {
      this.data2.pais = res.pais;
      this.data2.picture = res.picture;
    }, (error) => {
      console.error(error)
    })
  }

  //CARGAR USUARIO
  public getSingleEmpleado(id:any) {
    this.notificationService.alertLoading('Cargando...', 5000);
    this.secondService.getSingle(id)
    .subscribe((res) => {
      this.data = res;
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();
      console.error(error)
    })
  }

  //CAMBIAR CONTRASEÑA
  changePasswordProfile() {
    if(this.changePassword.old_pass == this.changePassword.new_pass) {
      this.notificationService.alertToast('No se puede usar la misma contraseña.');
    } else {
      if(this.changePassword.new_pass.length >= 8) {
        if(this.changePassword.new_pass == this.changePassword.new_pass_rep) {
          this.btnDisabled2 = true;
          this.updatePassword(this.changePassword)
        } else {
          this.notificationService.alertToast('Las contraseñas no coinciden.');
        }
      } else {
        this.notificationService.alertToast('La contraseña debe contener al menos 8 caracteres.');        
      }
    }
  }

  //CHANGE PASSWORD
  updatePassword(data:any) {
    this.mainService.changePassword(data)
    .subscribe((res) => {
      this.closeModal();
      this.notificationService.alertMessage('Contraseña Actualizada', 'La contraseña ha sido actualizada exitosamente.');
    },(error) => {
      this.btnDisabled2 = false;
      this.notificationService.alertToast('Contraseña inválida.')
    })
  }

  //CAMBIAR FOTO DE PERFIL
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}usuarios/${this.data2.id}/upload/avatar`;
    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
          $("#"+id).upload(url,
          {
            avatar: archivos[0]
          },
          function(respuesta)
          {
            $('#imgAvatar').attr("src",respuesta.picture)
            localStorage.setItem('currentPicture', respuesta.picture);
            $("#"+id).val('')
        });
    } else {
      this.notificationService.alertToast('El tipo de imagen no es válido.');
    }
  }

}
