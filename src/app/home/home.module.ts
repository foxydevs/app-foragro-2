import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page'; 
import { UbicacionService } from '../_services/ubicacion.service';
import { PaisService } from '../_services/pais.service';
import { ModalVisitaComponent } from './visita/modal-visita/modal-visita.component';
import { ModalPlanificacionComponent } from './planificacion/modal-planificacion/modal-planificacion.component';
import { TipoVisitaService } from '../_services/tipo-visita.service';
import { TipoVentaService } from '../_services/tipo-venta.service';
import { TipoClienteService } from '../_services/tipo-cliente.service';
import { TipoAvanceService } from '../_services/tipo-avance.service';
import { ObjetivoService } from '../_services/objetivo.service';
import { CultivoService } from '../_services/cultivo.service';
import { CamionService } from '../_services/camion.service';
import { ModalPerfilComponent } from './perfil/modal-perfil/modal-perfil.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      { path: '',
        component: HomePage },
      { path: 'visita', 
        loadChildren: './visita/visita.module#VisitaPageModule' },
      { path: 'planificacion', 
        loadChildren: './planificacion/planificacion.module#PlanificacionPageModule' },
      { path: 'cliente', 
        loadChildren: './cliente/cliente.module#ClientePageModule' },
      { path: 'perfil', 
        loadChildren: './perfil/perfil.module#PerfilPageModule' },
      { path: 'finalizado', 
        loadChildren: './finalizado/finalizado.module#FinalizadoPageModule' },
      { path: 'finalizado-detalle/:id', 
        loadChildren: './finalizado-detalle/finalizado-detalle.module#FinalizadoDetallePageModule' },
      { path: 'planificacion-detalle/:id', 
        loadChildren: './planificacion-detalle/planificacion-detalle.module#PlanificacionDetallePageModule' },
      { path: 'visita-detalle/:id', 
        loadChildren: './visita-detalle/visita-detalle.module#VisitaDetallePageModule' },
      { path: 'offline', 
        loadChildren: './offline/offline.module#OfflinePageModule' },
      { path: 'venta/:id', 
        loadChildren: './venta/venta.module#VentaPageModule' },
    ])
  ],
  declarations: [
    HomePage,
    ModalVisitaComponent,
    ModalPlanificacionComponent,
    ModalPerfilComponent
  ],
  entryComponents: [
    ModalVisitaComponent,
    ModalPlanificacionComponent,
    ModalPerfilComponent
  ],
  providers: [
    UbicacionService,
    PaisService,
    TipoVisitaService,
    TipoVentaService,
    TipoClienteService,
    TipoAvanceService,
    ObjetivoService,
    CultivoService,
    CamionService
  ]
})
export class HomePageModule {}
