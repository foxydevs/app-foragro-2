import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/_services/usuario.service';
import { NotificacionService } from 'src/app/_services/notificacion.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-recovery',
  templateUrl: './modal-recovery.component.html',
  styleUrls: ['./modal-recovery.component.scss'],
})
export class ModalRecoveryComponent implements OnInit {
  //PROPIEDADES
  private title:string = 'Recuperar Contraseña'
  pictureLogin:string = 'http://www.freejpg.com.ar/asset/900/ad/ad6b/F100007797.jpg';
  pictureApp:string = 'https://scontent-dfw5-1.xx.fbcdn.net/v/t1.0-9/28276976_1617297721658826_1473267187299341203_n.jpg?_nc_cat=106&_nc_ht=scontent-dfw5-1.xx&oh=9f20b34933a901eeb69fc52214e947f8&oe=5D8D7351';
  private btnDisabled:boolean;
  private data = {
    username: ''
  }
  constructor(private mainService: UsuarioService,
    private notificationService: NotificacionService,
    private modalController: ModalController) { }

  ngOnInit() {
  }

  //RECOVERY
  public resetPassword() {
    if(this.data.username) {
      this.btnDisabled = true;
      this.mainService.resetpassword(this.data)
      .subscribe((res) => {
        this.notificationService.alertMessage('Contraseña Recuperada', 'Te hemos enviado un correo a: ' + res.email);
        this.closeModal();
      }, (error) => {
        this.notificationService.alertToast('Usuario no encontrado.');
        this.btnDisabled = false;
        console.log(error)
      })
    } else {
      this.notificationService.alertToast('El correo es requerido.');
    }
  }

  //CERRAR MODAL
  closeModal(alert?:any) {
    this.modalController.dismiss(alert);
  }

}
