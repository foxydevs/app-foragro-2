import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, ModalController } from '@ionic/angular';
import { AuthService } from '../_services/auth.service';
import { NotificacionService } from '../_services/notificacion.service';
import { ModalRecoveryComponent } from './modal-recovery/modal-recovery.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  //PROPIEDADES
  pictureLogin:string = 'http://www.freejpg.com.ar/asset/900/ad/ad6b/F100007797.jpg';
  pictureApp:string = 'https://bpresentacion.s3-us-west-2.amazonaws.com/Foragro/foragro.png';
  private disabledBtn:boolean = false;
  private passwordType:string = 'password';
  private passwordShow:boolean = false;
  private data = {
    username: '',
    password: ''
  }
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    private mainService: AuthService,
    private notificationService: NotificacionService,
    private modalController: ModalController
  ) { }

  //SIGUIENTE PAGINA
  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  //TOGGLE PASSWORD
  togglePassword() {
    if(this.passwordShow) {
      this.passwordShow = false;
      this.passwordType = 'password';
    } else {
      this.passwordShow = true;
      this.passwordType = 'text';
    }
  }

  //INICIAR SESIÓN
  logIn(){
    this.mainService.auth(this.data)
    .subscribe((res) => {
      localStorage.setItem('currentUser', res.username);
      localStorage.setItem('currentForagro', res.username);
      localStorage.setItem('currentId', res.id);
      if(res.empleados) {
        localStorage.setItem('currentFirstName', res.empleados.nombre);
        localStorage.setItem('currentLastName', res.empleados.apellido);
      }
      localStorage.setItem('currentPicture', res.picture);
      localStorage.setItem('currentEmail', res.email);
      localStorage.setItem('currentState', res.estado);
      localStorage.setItem('currentEmpleadoID', res.empleado);
      localStorage.setItem('currentCamiones', JSON.stringify(res.camiones));
      this.goToRoute('home');
    }, error => {
      this.notificationService.alertToast('Usuario o contraseña incorrectos.');
    });
  }

  ngOnInit() {
  }

  //ABRIR MODAL
  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalRecoveryComponent
    });
    modal.onDidDismiss().then((data) => {
    });
    return await modal.present();
  }

}
